#!usr/bin/env python3

import sys
import json
import math

modelData = []
varDict = {}

def read_model_file():
	
	spamOrSentiment = sys.argv[1].split('.')
	if(spamOrSentiment[0] == "spam"):
		classifySpamModel()
	else:
		classifySentimentModel()
		
def classifyModel():
	global varDict
	modelFile = open(sys.argv[1],"r")
	modelData = json.loads(modelFile.read())
	modelFile.close()
	classList = modelData[0]["classList"]
	learnt_no_of_words = modelData[1]["total_no_of_words"]
	i=2
	for x in classList:
		key = "prior"+x
		varDict[key]= modelData[i][x+"priorProb"]
		i=i+1
		key = "learnt_no_of_"+x+"words"
		varDict[key]= modelData[i]["total_no_of_"+x+"words"]
		i=i+1
		key = x+"Dict"
		varDict[key]= modelData[i][x+"ProbDict"]
		i=i+1
	read_test_file(classList,varDict,learnt_no_of_words)

def read_test_file(classList,varDict,learnt_no_of_words):
	testFile = open(sys.argv[2],"r")
	'''if "SPAM" in classList:
		Output = open("spam.out","w")
	elif "POS" in classList:
		Output = open("sentiment.out","w")
	else:
		Output = open("x.out","w")'''
	docsList = testFile.readlines()
		
	for x in docsList:
		for y in classList:
			msgWordList = x.split()
			key = y+"Prob" 
			varDict[key] = calcMsgProb(msgWordList,varDict["prior"+y],learnt_no_of_words,varDict["learnt_no_of_"+y+"words"],varDict[y+"Dict"])

		probList = []
		for y in classList:
			probList.append(varDict[y+"Prob"])
		
		keyIndex = probList.index(max(probList))
				
			
		#Output.write(classList[keyIndex]+"\n")
		print(classList[keyIndex])
		
	testFile.close()
	
def calcMsgProb(msgWordList,prior,learnt_no_of_words,learnt_no_of_Classwords,Dict):
	probMsgClass = 0

	for word in msgWordList:
		wordSplit = word.split('\r')
		if wordSplit[0] in Dict:
			probMsgClass +=float(Dict[wordSplit[0]])
		else:
			probMsgClass += math.log(1/(int( learnt_no_of_Classwords)+int(learnt_no_of_words)))
	probMsgClass += float(prior)
	return probMsgClass


def main():
	classifyModel()


main()
