#!usr/bin/env python3

import codecs
import sys
import os

def read_dir_Training(inputFilePath):
	
	print(inputFilePath)
	ofile = open("sentiment_training.txt","wt")
	for root,dirs, ifileList in os.walk(inputFilePath):
		for inpfile in ifileList:
			filename = inpfile.split('.')[0]
			ofile.write(filename+" ")
			with open(inputFilePath+"/"+inpfile,"r",errors="ignore") as ipfile:
				lines= ipfile.read()
				txtlines = lines.splitlines()
				ofile.write(str(' '.join(txtlines))+"\n")
	ofile.close()
		
read_dir_Training(sys.argv[1])


	
