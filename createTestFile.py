import codecs
import sys
import os

def read_dir_spamTraining(inputFilePath):
	
	ofile = open("actual_sentiment_test.txt","wt")
	#opfile = open("checkSentiment.txt","w")
	for root,dirs, ifileList in os.walk(inputFilePath):
		for inpfile in sorted(ifileList):
			#filename = inpfile.split('.')[0]
			#opfile.write(filename+"\n")
			with open(inputFilePath+"/"+inpfile,"r",errors="ignore") as ipfile:
				lines= ipfile.read()
				txtlines = lines.splitlines()
				ofile.write(str(' '.join(txtlines))+"\n")
	ofile.close()
	#opfile.close()
		
read_dir_spamTraining(sys.argv[1])
