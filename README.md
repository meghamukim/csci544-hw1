PART 1:

HAM label:
Precision:0.98
Recall:0.98
F-Score: 0.98

SPAM label:
Precision:0.95
Recall:0.96
F-Score:0.96

POS label:
Precision:0.80
Recall:0.81
F-Score:0.81

NEG label:
Precision:0.86
Recall:0.85
F-Score:0.86

------------------------------------------------------

PART 2:

SVM

HAM label:
Precision:0.93
Recall:0.99
F-Score:0.95

SPAM label:
Precision:0.96
Recall:0.80
F-Score:0.86

POS label:
Precision:0.80
Recall:0.90
F-Score:0.85

NEG label:
Precision:0.92
Recall:0.84
F-Score:0.88

------------------

MegaM

HAM label:
Precision:0.99
Recall:0.99
F-Score:0.99

SPAM label:
Precision:0.96
Recall:0.97
F-Score:0.97

POS label:
Precision:0.80
Recall:0.87
F-Score:0.84

NEG label:
Precision:0.90
Recall:0.85
F-Score:0.87

----------------------------------------------------------------

PART 3:

HAM label:
Precision:0.95
Recall:0.93
F-Score: 0.94

SPAM label:
Precision:0.93
Recall:0.93
F-Score:0.93

POS label:
Precision:0.76
Recall:0.79
F-Score:0.78

NEG label:
Precision:0.80
Recall:0.79
F-Score:0.80

---------------------

SVM

HAM label:
Precision:0.99
Recall:0.80
F-Score:0.88

SPAM label:
Precision: 0.90
Recall: 0.45
F-Score: 0.60

POS label:
Precision: 0.74
Recall: 0.76
F-Score: 0.75

NEG label:
Precision: 0.75
Recall: 0.76
F-Score: 0.76

-------------------------
MegaM

HAM label: 
Precision: 0.97
Recall: 0.97
F-Score: 0.97

SPAM label:
Precision: 0.93
Recall: 0.92
F-Score: 0.93

POS label:
Precision:0.80
Recall:0.78
F-Score:0.79

NEG label:
Precision: 0.78
Recall: 0.80
F-Score: 0.79

If we reduce the training data to 10% of the training data previously used, then the recall,precision and fscore will reduce. But this reduction will not be too large. This might be because the classifier has already learnt enough and any more data will not affect its learning curve. After a certain point the learning curve might go straight, unaffected by any amount of training data.

--------------------------------------------------------------------