#!usr/bin/env python3

import sys
import math
import json

hamDictionary = {}
spamDictionary = {}
probHam = 0
probSpam = 0
hamProbDict = {}
spamProbDict = {}

posDictionary = {}
negDictionary = {}
probPos = 0
probNeg = 0
posProbDict = {}
negProbDict = {}

variablesDict = {}


def learnData():
	tr_file = open(sys.argv[1],"r")
	tr_list = tr_file.readlines()
	#stopWordsList = ["and","her","she","he","him","is","it","of","on","my","or","our","them","their","a","an","the","for","thanks","regards","hi","hello","I","you","bye","subject:","subject","we","was","is"]
	classList = []
	global variablesDict
	no_of_docs=0
	total_no_of_words = 0
	
	for x in tr_list:
		email = x.split()
		#if email[0].lower() not in stopWordsList:
		if email[0] in classList:
			variablesDict.update(addWordToClass(email[1:],email[0],variablesDict,True))
		else:
			classList.append(email[0])
			variablesDict.update(addWordToClass(email[1:],email[0],variablesDict,False))

	for x in classList:
		no_of_docs += variablesDict["no_of_"+x+"_docs"]
		total_no_of_words += len(variablesDict[x+"Dictionary"])

	variablesDict["no_of_docs"] = no_of_docs
	variablesDict["total_no_of_words"] = total_no_of_words

	for x in classList:
		key= x+"priorProb"
		variablesDict[key]= calcPriorProbability(variablesDict["no_of_docs"],variablesDict["no_of_"+x+"_docs"])
		key = x+"ProbDict"
		variablesDict[key]= calcProbDict(variablesDict[x+"Dictionary"],variablesDict["total_no_of_words"],variablesDict["total_no_of_"+x+"words"]) 
	
	writeDataToModel(classList,variablesDict)
	
	tr_file.close()

def calcPriorProbability(totalCount, specificCount):
	return specificCount/totalCount

def calcProbDict(Dictionary,totalWords,totalClassWords):
	ProbDict= {}
	Keys =  Dictionary.keys()
	for key in Keys:
		ProbDict[key] = math.log((Dictionary[key]+1)/(totalClassWords+totalWords))
	
	return ProbDict
	
def writeDataToModel(classList,variablesDict):
	ofile = open(sys.argv[2],"w")
	data= [{"classList":classList},{"total_no_of_words": str(variablesDict["total_no_of_words"])}]
	for x in classList:
		data.append({x+"priorProb":str(variablesDict[x+"priorProb"])})
		data.append({"total_no_of_"+x+"words":str(variablesDict["total_no_of_"+x+"words"])})
		data.append({x+"ProbDict":variablesDict[x+"ProbDict"]})
	
	json.dump(data,ofile)	
	ofile.close()


def addWordToClass(dataList,className,variablesDict,present):
	key = "total_no_of_"+className+"words"
	if present:
		variablesDict[key] += len(dataList)
	else:
		variablesDict[key] = len(dataList)

	key = className+"Dictionary"
	if not present:
		variablesDict[key] = {}
		
	for word in dataList:
		wordSplit = word.split('\r')
		if wordSplit[0] in variablesDict[key]:
			variablesDict[key][wordSplit[0]] += 1
		else:
			variablesDict[key][wordSplit[0]] = 1
	key= "no_of_"+className+"_docs"
	if present:
		variablesDict[key] += 1
	else:
		variablesDict[key] = 1
	return variablesDict
	
	tr_file.close()

	
	
def main():
	learnData()

	
main()
